FROM ubuntu:22.04

# Dependencies
RUN \
      apt-get update && \
      apt-get install -y --no-install-recommends \
      git \
      cmake \
      build-essential \
      make \
      libpcre2-dev \
      libssl-dev \
      libssh-dev \
      libcurl4-openssl-dev \
      pkg-config \
      wget \
      ca-certificates

# git SSL setup
RuN \
    mkdir -p /usr/local/share/ca-certificates/cacert.org && \
    wget -P /usr/local/share/ca-certificates/cacert.org http://www.cacert.org/certs/root.crt http://www.cacert.org/certs/class3.crt && \
    update-ca-certificates && \
    git config --global http.sslCAinfo /etc/ssl/certs/ca-certificates.crt

# Adding netconf user
RUN adduser --system netconf
RUN mkdir -p /home/netconf
RUN echo "netconf:netconf" | chpasswd && adduser netconf sudo

# Updating shell to bash
RUN sed -i s#/home/netconf:/bin/false#/home/netconf:/bin/bash# /etc/passwd

RUN mkdir /opt/dev && chown -R netconf /opt/dev

# set root password to root
RUN echo "root:root" | chpasswd

# libyang
RUN \
      cd /opt/dev && \
      git clone https://github.com/CESNET/libyang.git && cd libyang && \
      git checkout tags/v2.1.148 && \
      mkdir build && cd build && \
      cmake -DCMAKE_BUILD_TYPE:String="Release" -DCMAKE_INSTALL_PREFIX:PATH=/usr -DENABLE_BUILD_TESTS=OFF .. && \
      make -j2 && \
      make install

# libnetconf2
RUN \
      cd /opt/dev && \
      git clone https://github.com/CESNET/libnetconf2.git && cd libnetconf2 && \
      git checkout tags/v3.0.8 && \
      mkdir build && cd build && \
      cmake  -DCMAKE_BUILD_TYPE:String="Release" -DCMAKE_INSTALL_PREFIX:PATH=/usr -DENABLE_BUILD_TESTS=OFF .. && \
      make -j2 && \
      make install

# sysrepo
RUN \
      cd /opt/dev && \
      git clone https://github.com/sysrepo/sysrepo.git && cd sysrepo && \
      git checkout tags/v2.2.150 && \
      mkdir build && cd build && \
      cmake -DENABLE_TESTS=OFF -DCMAKE_BUILD_TYPE="Release" -DCMAKE_INSTALL_PREFIX:PATH=/usr -DREPOSITORY_LOC:PATH=/etc/sysrepo .. && \
      make -j2 && \
      make install

# netopeer 2
RUN \
      cd /opt/dev && \
      git clone https://github.com/CESNET/netopeer2.git && cd netopeer2 && \
      git checkout tags/v2.2.13 && \
      mkdir build && cd build && \
      cmake -DCMAKE_BUILD_TYPE:String="Release" -DENABLE_TESTS=OFF -DCMAKE_INSTALL_PREFIX:PATH=/usr .. && \
      make -j2 && \
      make install

# netopeer2 TLS configuration
# See http://www.seguesoft.com/index.php/netconfc/free-trial-netconfc/2-uncategorised/1-setup-netopeer2
RUN \
      cd /opt/dev/netopeer2/example_configuration && \
      sysrepocfg -v3 --edit=tls_keystore.xml --format=xml --datastore=running --module=ietf-keystore && \
      sysrepocfg -v3 --edit=tls_truststore.xml --format=xml --datastore=running --module=ietf-truststore && \
      sysrepocfg -v3 --edit=tls_listen.xml --format=xml --datastore=running --module=ietf-netconf-server && \
      sysrepocfg -v3 --copy-from=running --datastore=startup

EXPOSE 6513
CMD ["/usr/sbin/netopeer2-server", "-d", "-v3"]
